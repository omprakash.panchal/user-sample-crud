package com.userSampleProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserSampleProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserSampleProjectApplication.class, args);
	}

}
