package com.userSampleProject.controllers;

import com.userSampleProject.entity.User;
import com.userSampleProject.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class UserController {

    @Autowired
    private UserService userService;


    //Create User
    @PostMapping("/users")
    public ResponseEntity<User> addUSer(@RequestBody User user)
    {
        return new ResponseEntity<>(userService.createUser(user), HttpStatus.CREATED) ;
    }

    //Get All Users
    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers()
    {
        return new ResponseEntity<>(userService.getUser(),HttpStatus.OK);
    }

    //Get USerBy Id
    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUserByID(@PathVariable int id)
    {
        return  new ResponseEntity<>(userService.getUseById(id),HttpStatus.FOUND);
    }

    //Delete User By Id
    @DeleteMapping("/users/delete/{id}")
    public void deleteUser(@PathVariable int id)
    {
        userService.deleteUser(id);
    }

    //Update User
    @PutMapping("/users/update/{id}")
    public User updateUSer(@PathVariable int id,User user)
    {
        return  userService.updateUSer(id,user);
    }




}
