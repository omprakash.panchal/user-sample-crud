package com.userSampleProject.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class User {
    int id;
    String firstName;
    String middleName;
    String lastName;
    String email;
    double contactNumber;
    String address;

    public User(String firstName, String middleName, String lastName, String email, double contactNumber, String address) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.email = email;
        this.contactNumber = contactNumber;
        this.address = address;
    }
}
