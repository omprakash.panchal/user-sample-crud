package com.userSampleProject.services;

import com.userSampleProject.entity.User;

import java.util.List;

public interface UserService {

    //Create
    public  User createUser(User user);

    //Delete
    public void deleteUser(int id);

    //getAll
    public  List<User> getUser();
    //getByID

    //get user by id
    public User getUseById(int id);

    //Update
     public User updateUSer(int id,User user);


}
