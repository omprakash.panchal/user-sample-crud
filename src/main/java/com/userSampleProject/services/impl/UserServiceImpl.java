package com.userSampleProject.services.impl;

import com.userSampleProject.entity.User;
import com.userSampleProject.services.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class UserServiceImpl implements UserService {

    // For Inserting And Getting Static Values
    private static Map<Integer, User> users = new HashMap<>();
    int incrsId = 1;

    static {
        User user1 = new User("omprakash", "Bhimrao", "Panchal", "om@gmail.com", 123456778, "solapur");
        users.put(1, user1);
    }

    //Create User

    public User createUser(User user) {

        incrsId += 1;
        user.setId(incrsId);
        users.put(incrsId, user);
        return user;
    }

    //Delete User

    public void deleteUser(int id) {
        users.remove(id);

    }

    //Get All Users

    public List<User> getUser() {

        return new ArrayList<>(users.values());
    }

    //Get User By Id

    public User getUseById(int id) {

        return users.get(id);
    }

    //Update User By Id
    public User updateUSer(int id, User user) {
        user.setId(id);
       User updatedUser =users.put(id, user);
        return updatedUser;
    }
}
